using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _venda;
        public VendaController(VendaContext venda)
        {
            _venda = venda;
        }

        [HttpPut("AtualizarStatus/{id}")]
        public IActionResult AtualizarStatusVenda(int id, EnumStatusVenda statusVenda)
        {
            var vendaBanco = _venda.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();  


            if ((vendaBanco.Status == EnumStatusVenda.Aguardando_Pagamento && statusVenda == EnumStatusVenda.Pagamento_Aprovado) || 
                (vendaBanco.Status == EnumStatusVenda.Aguardando_Pagamento && statusVenda == EnumStatusVenda.Cancelada) ||
                (vendaBanco.Status == EnumStatusVenda.Pagamento_Aprovado && statusVenda == EnumStatusVenda.Enviado_Para_Transportadora) ||
                (vendaBanco.Status == EnumStatusVenda.Pagamento_Aprovado && statusVenda == EnumStatusVenda.Cancelada) ||
                (vendaBanco.Status == EnumStatusVenda.Enviado_Para_Transportadora && statusVenda == EnumStatusVenda.Entregue))
            {
                vendaBanco.Status = statusVenda;
            }
            else if (vendaBanco.Status == statusVenda)
            {
                return BadRequest($"A Venda \"{vendaBanco.Id}\", já se encontra com o status \"{statusVenda}\". Por Favor selecione um novo status.");
            }
            else 
            {
                return BadRequest($"O status \"{vendaBanco.Status}\", não pode ser alterado diretamente para o status \"{statusVenda}\". Por Favor selecione um status válido.");
            }
            
            
            _venda.Vendas.Update(vendaBanco);
            _venda.SaveChanges();

            return CreatedAtAction(nameof(ObterVendaPorId), new { id = vendaBanco.Id }, vendaBanco);
        }


        [HttpGet("Consultar/{id}")]
        public IActionResult ObterVendaPorId(int id)
        {
            var consultaVenda = _venda.Vendas.Find(id);
            var consultaVendedor = _venda.Vendedores.Where(x => x.Id == consultaVenda.IdVendedor).ToList();
            List<ItemVenda> consultaItens = _venda.ItensVendas.Where(x => x.IdVenda == consultaVenda.Id).ToList();

            if (consultaVenda == null)
                return NotFound();

            var response = new ConsultaVenda {
                Id = consultaVenda.Id,
                Data = consultaVenda.Data,
                Status = consultaVenda.Status,
                IdVendedor = consultaVenda.IdVendedor,
                NomeVendedor = consultaVendedor[0].Nome,
                CPF = consultaVendedor[0].Cpf,
                EmailVendedor = consultaVendedor[0].Email,
                Telefone = consultaVendedor[0].Telefone,
                Itens = consultaItens
            };

            return Ok(response);
        }

        [HttpPost("Cadastrar/{idVendedor}")]
        public IActionResult RegistrarVenda(int idVendedor,  List<ItemVenda> itensVenda )
        {
            var vendaBanco = new Venda();
            var vendedorBanco = _venda.Vendedores.Find(idVendedor);

            if (idVendedor == 0)
                return BadRequest(new { Erro = "O campo vendedor não pode ser vazio!" });

            if (vendedorBanco == null)
                return BadRequest(new { Erro = "Vendedor não encontrado!" });

            if (itensVenda.Count == 0 || itensVenda[0].Nome.Trim() == "" || itensVenda[0].Nome == null || itensVenda[0].Nome == "string")
                return BadRequest(new { Erro = "A venda deve conter pelo menos 1 item!" });
            
            if (itensVenda[0].Valor <= 0)
                return BadRequest(new { Erro = "O Valor do item tem que ser maior que zero" });

            // Cria uma nova venda
            vendaBanco.IdVendedor = vendedorBanco.Id;
            vendaBanco.Data = DateTime.Now;
            vendaBanco.Status = EnumStatusVenda.Aguardando_Pagamento;

            _venda.Add(vendaBanco);
            _venda.SaveChanges();

            //Adiciona os itens à venda criada,
            foreach (var item in itensVenda)
            {
                if (item.Valor > 0)
                {
                    item.IdVenda = vendaBanco.Id;

                    if (item.Quantidade == 0)
                        item.Quantidade = 1;

                    _venda.ItensVendas.Add(item);
                    _venda.SaveChanges();
                }
                else
                {
                    continue;
                }
            }

            
            return CreatedAtAction(nameof(ObterVendaPorId), new { id = vendaBanco.Id }, vendaBanco);
        }
    }
}