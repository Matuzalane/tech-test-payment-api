using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly VendaContext _vendedor;

        public VendedorController(VendaContext vendedor)
        {
            _vendedor = vendedor;
        }

        [HttpPost("Cadastrar")]
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {
            _vendedor.Vendedores.Add(vendedor);
            _vendedor.SaveChanges();

            return CreatedAtAction(nameof(ConsultarVendedor), new { id = vendedor.Id }, vendedor);
        }
        
        [HttpGet("Consultar")]
        public IActionResult ConsultarVendedor(int id)
        {
            var vendedor = _vendedor.Vendedores.Find(id);
        
            if (vendedor == null)
            {
                return NotFound();
            }

            return Ok(vendedor);
        }

        [HttpPut("{id}")]
        public IActionResult Atualizar(int id, Vendedor vendedor)
        {
            var dbVen = _vendedor.Vendedores.Find(id);

            if (dbVen == null)
                return NotFound();

            if (vendedor.Nome.Trim() != "" && vendedor.Nome != null && vendedor.Nome != "string")
            {
                dbVen.Nome = dbVen.Nome != vendedor.Nome ? dbVen.Nome = vendedor.Nome : dbVen.Nome;
            }

            if (vendedor.Email.Trim() != "" && vendedor.Email != null && vendedor.Email != "string")
            {
                dbVen.Email = dbVen.Email != vendedor.Email ? dbVen.Email = vendedor.Email : dbVen.Email;
            }

            if (vendedor.Cpf.Trim() != "" && vendedor.Cpf != null && vendedor.Cpf != "string")
            {
                dbVen.Cpf = dbVen.Cpf != vendedor.Cpf ? dbVen.Cpf = vendedor.Cpf : dbVen.Cpf;
            }

            if (vendedor.Telefone.Trim() != "" && vendedor.Telefone != null && vendedor.Telefone != "string")
            {
                dbVen.Telefone = dbVen.Telefone != vendedor.Telefone ? dbVen.Telefone = vendedor.Telefone : dbVen.Telefone;
            }
            
            
            _vendedor.Vendedores.Update(dbVen);
            _vendedor.SaveChanges();

            return Ok(dbVen);
        }

        [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var dbVen = _vendedor.Vendedores.Find(id);

            if (dbVen == null)
                return NotFound();

            _vendedor.Vendedores.Remove(dbVen);
            _vendedor.SaveChanges();

            return NoContent();
        }
    }
}