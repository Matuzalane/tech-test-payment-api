namespace tech_test_payment_api.Models
{
    public enum EnumStatusVenda
    {
        // id: 0
        Aguardando_Pagamento,
        // id: 1
        Pagamento_Aprovado,
        // id: 2
        Enviado_Para_Transportadora,
        // id: 3
        Entregue,
        // id: 4
        Cancelada

    }
}