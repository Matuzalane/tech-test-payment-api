using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Models
{
    public class ConsultaVenda
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda Status { get; set; }
        public int IdVendedor { get; set; }
        public string NomeVendedor { get; set; }
        public string CPF { get; set; }
        public string EmailVendedor { get; set; }
        public string Telefone { get; set; }
        public List<ItemVenda> Itens { get; set; }
    }
}